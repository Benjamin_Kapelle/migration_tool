SELECT
        concat("Address, telephone data.".ikontrah,trim("Address, telephone data.".iadres)) as "PrimaryKey"
  , "Database of Partners".ikontrah                                                         as "Company"
  , "Database of Partners".skrot                                                            as "Abbrev. co. name"
  , "Database of Partners".nazwa                                                            as "Company name"
  , "Database of Partners".iformy_o                                                         as "Org form"
  , "Database of Partners".eksporter                                                        as "Export ID"
  , "Database of Partners".ipriofin                                                         as "Accounting priority"
  , "Database of Partners".forgan                                                           as "Reg. body"
  , "Database of Partners".fdostawc                                                         as "Supplier"
  , "Database of Partners".fmarketi                                                         as "Marketing"
  , "Address, telephone data.".iadres                                                       as "Addr ID"
  , "Address, telephone data.".uadres                                                       as "Recipient status"
  , "Address, telephone data.".typ_adresu                                                   as "Address type"
  , "Address, telephone data.".ikraje                                                       as "Country"
  , "Address, telephone data.".nipue                                                        as "EU VAT ID"
  , "Address, telephone data.".ulica                                                        as "Street"
  , "Address, telephone data.".dom                                                          as "Home"
  , "Address, telephone data.".mieszkanie                                                   as "Loc."
  , "Address, telephone data.".skrytka_p                                                    as "PO Box"
  , "Address, telephone data.".ikody                                                        as "Zip Code"
  , "Address, telephone data.".ipoczty                                                      as "Post"
  , "Address, telephone data.".imiejsco                                                     as "Town"
  , "Address, telephone data.".iwojewod                                                     as "Province"
  , "Address, telephone data.".ipowiaty                                                     as "District"
  , "Address, telephone data.".igminy                                                       as "Commune"
  , "Address, telephone data.".idgminy                                                      as "Previous id"
  , "Address, telephone data.".czy_adres                                                    as "Edit address"
  , "Address, telephone data.".adres                                                        as "Partner's data"
  , "Address, telephone data.".telefon                                                      as "Phone"
  , "Address, telephone data.".telefond                                                     as "Phone 2"
  , "Address, telephone data.".telefonk                                                     as "Phone 3"
  , "Address, telephone data.".telex                                                        as "Telex number"
  , "Address, telephone data.".telefax                                                      as "Fax number"
  , "Address, telephone data.".e_mail                                                       as "e-mail"
  , "Address, telephone data.".www                                                          as "Website"
  , "Address, telephone data.".czy_inna                                                     as "Edit name"
  , "Address, telephone data.".opis                                                         as "Address desc."
  , "Address, telephone data.".ikontrah1                                                    as "As a partner"
  , "Address, telephone data.".iln                                                          as "ILN"
  , "Address, telephone data.".islownik                                                     as "Index of description in a"
  , "Address, telephone data.".kto                                                          as "Who"
  , "Address, telephone data.".kiedy                                                        as "When"
  , "Address, telephone data.".minuta                                                       as "Time"
  , "Address, telephone data.".samofakt                                                     as "Self-invoicing?"
  , "Dictonary of partners".ukontrah                                                        as "Trading partner status"
  , "Dictonary of partners".ikontrah                                                        as "Partner ID"
  , "Dictonary of partners".izaklady                                                        as "Plant ID"
  , "Dictonary of partners".info                                                            as "Partner code"
  , "Dictonary of partners".iopis                                                           as "Unused field"
  , "Dictonary of partners".nip                                                             as "VAT ID"
  , "Dictonary of partners".nip10                                                           as "VAT ID (without breaks)"
  , "Dictonary of partners".platnikvat                                                      as "Sales document type"
  , "Dictonary of partners".uwagi                                                           as "Additional comments"
  , "Dictonary of partners".pocz_z                                                          as "Date of creating a partner"
  , "Dictonary of partners".opis                                                            as "Partner name"
  , "Dictonary of partners".opis2                                                           as "Additional description of Company"
  , "Dictonary of partners".fosoby                                                          as "Person"
  , "Dictonary of partners".flisty                                                          as "List of payments"
  , "Dictonary of partners".fplatnik                                                        as "Payer"
  , "Dictonary of partners".fzaloga                                                         as "Package users"
  , "Dictonary of partners".fplantat                                                        as "Planter"
  , "Dictonary of partners".finspekt                                                        as "Inspector"
  , "Dictonary of partners".wojskowy                                                        as "Covered by the obligation"
  , "Dictonary of partners".fprenum                                                         as "Subscriber"
  , "Dictonary of partners".fwysyl                                                          as "Shipping"
  , "Dictonary of partners".fzgoda                                                          as "Information about the sub"
  , "Dictonary of partners".bok                                                             as "Customer/Code 1"
  , "Dictonary of partners".bok2                                                            as "Customer/Code 2"
  , "Dictonary of partners".islownik                                                        as "Id of description in diff"
  , "Dictonary of partners".idowody_w                                                       as "Document archive id"
  , "Dictonary of partners".kto                                                             as "Who"
  , "Dictonary of partners".kiedy                                                           as "When"
  , "Dictonary of partners".minuta                                                          as "Time"
  , "Dictonary of partners"."timestamp"                                                     as "Time of last modification"
  , coalesce("Payment Terms".termin_def, '14')                                              as "Payment Terms Code"
  , coalesce("Payment Terms"."Payment Method", 'P')                                         as "Payment Method"
  , "Payment Terms".splatnex                                                                as "Payment Method2"
  , "Credit Limit".konc                                                                     as "Limit date"
  , "Credit Limit".kwota                                                                    as "Limit"
  , concat("Address, telephone data.".ikontrah,trim("Address, telephone data.".iadres)) as "CustID"
  , "KeyWords"."CustomerClassificationID"                                                   as "CustomerClassificationID"
  , "Segments"."SALESSEGMENTID"                                                             as "SALESSEGMENTID"
  , "Segments"."LINEOFBUSINESSID"                                                           as "LINEOFBUSINESSID"
  , "Segments"."COMPANYCHAIN"                                                               as "COMPANYCHAIN"
  , "Persons".isprzeda                                                                      as "Salespersone Code"
  , case
        when trim("Person Names".imie2) is null
            then concat(trim("Person Names".imie),' ', trim("Person Names".nazwisko))
            else concat(trim("Person Names".imie), ' ', trim("Person Names".imie2),' ', trim("Person Names".nazwisko))
    end                     as "Full Name"
  , "Person Names".nazwisko as "Last Name"
  , "Person Names".imie     as "First Name"
  , "Person Names".imie2    as "First Name2"
FROM
    fk.dfirmy as "Database of Partners"
    left join
        fk.dadres as "Address, telephone data."
        on
            "Address, telephone data.".ikontrah = "Database of Partners".ikontrah
    left join
        (
            select distinct
            on
                (
                    ikontrah
                )
                ikontrah
              , "Payment Conditions".termin_def
              , coalesce("Payment Conditions"."PM_1", 'P') as "Payment Method"
              , splatnex
            from
                fk.dplat_h
                left join
                    (
                        select
                            termin_def
                          , coalesce(iplatnex, 'P') as "PM_1"
                        from
                            fk.dplatnex
                    )
                    as "Payment Conditions"
                    on
                        "Payment Conditions"."PM_1" = fk.dplat_h.iplatnex
            where
                termin is not null
            order by
                ikontrah
              , Termin
              , konc desc
        )
        as "Payment Terms"
        on
            "Payment Terms".ikontrah = "Database of Partners".ikontrah
    inner join
        (
            select distinct
                    concat(ikontrah, coalesce(iadres, 'A')) as "PrimaryKey_Invoice_A"
            from
                fk.dfaktury
            where
                ikontrah is not null
        )
        as "Sales Invoice_A"
        on
                concat("Address, telephone data.".ikontrah, iadres) = "Sales Invoice_A"."PrimaryKey_Invoice_A"
    left join
        fk.dkontrah as "Dictonary of partners"
        on
            "Dictonary of partners".ikontrah = "Database of Partners".ikontrah
    left join
        (
            select distinct
            on
                (
                    ikontrah
                )
                "CL".ikontrah
              , "CL".typumowy
              , "CL".konc
              , "CL".kwota
            FROM
                fk.dumowy as "CL"
            where
                typumowy    = 'K'
                or typumowy = 'k'
            order by
                ikontrah
              , konc desc
        )
        as "Credit Limit"
        on
            "Database of Partners".ikontrah = "Credit Limit".ikontrah
    left join
        (
            select
                    concat("KW".ikontrah, "KW".iadres) as "PrimaryKey"
              , 'Key Account'::text                    as "CustomerClassificationID"
            FROM
                fk.dhasla as "KW"
            where
                ihaslax     = 'H_key accounts'
                and ihaslas = '1'
                and konc   >= current_date
        )
        as "KeyWords"
        on
                concat("Address, telephone data.".ikontrah, iadres) = "KeyWords"."PrimaryKey"
    left join
        (
            select distinct
            on
                (
                    ikontrah
                  , iadres
                )
                    concat(ikontrah, trim(iadres)) as "PrimaryKey"
              , case
                    when ihaslax is not null
                        and ihaslax        = 'H_segmentacja'
                        and konc          >= current_date
                        then ihaslas
                end as "SALESSEGMENTID"
              , case
                    when ihaslax is not null
                        and ihaslax        = 'H_kategoria'
                        and konc          >= current_date
                        then ihaslas
                end as "LINEOFBUSINESSID"
              , case
                    when ihaslax is not null
                        and ihaslax        = 'O_kryt_2'
                        and konc          >= current_date
                        then ihaslas
                end as "COMPANYCHAIN"
            FROM
                fk.dhasla as "Seg"
            where
                ihaslax in ('H_segmentacja'
                          , 'H_kategoria'
                          , 'O_kryt_2')
        )
        as "Segments"
        on
                concat("Address, telephone data.".ikontrah, iadres) = "Segments"."PrimaryKey"
    left join
        fk.dadres_h as "Persons"
        on
                concat("Persons".ikontrah, trim("Persons".iadres)) = concat("Address, telephone data.".ikontrah,trim("Address, telephone data.".iadres))
    left join
        fk.dsprzeda as "Sales Persons"
        on
            "Sales Persons".isprzeda = "Persons".isprzeda
    left join
        fk.dosoby as "Person Names"
        on
            "Sales Persons".ikontrah = "Person Names".ikontrah
where
        concat("Address, telephone data.".ikontrah, "Address, telephone data.".iadres) <> ''
    and "Persons".konc                                                                 >= current_date
union all
SELECT
        concat("Address, telephone data.".ikontrah,trim("Address, telephone data.".iadres)) as "PrimaryKey"
  , "Database of Partners".ikontrah                                                         as "Company"
  , "Database of Partners".skrot                                                            as "Abbrev. co. name"
  , "Database of Partners".nazwa                                                            as "Company name"
  , "Database of Partners".iformy_o                                                         as "Org form"
  , "Database of Partners".eksporter                                                        as "Export ID"
  , "Database of Partners".ipriofin                                                         as "Accounting priority"
  , "Database of Partners".forgan                                                           as "Reg. body"
  , "Database of Partners".fdostawc                                                         as "Supplier"
  , "Database of Partners".fmarketi                                                         as "Marketing"
  , "Address, telephone data.".iadres                                                       as "Addr ID"
  , "Address, telephone data.".uadres                                                       as "Recipient status"
  , "Address, telephone data.".typ_adresu                                                   as "Address type"
  , "Address, telephone data.".ikraje                                                       as "Country"
  , "Address, telephone data.".nipue                                                        as "EU VAT ID"
  , "Address, telephone data.".ulica                                                        as "Street"
  , "Address, telephone data.".dom                                                          as "Home"
  , "Address, telephone data.".mieszkanie                                                   as "Loc."
  , "Address, telephone data.".skrytka_p                                                    as "PO Box"
  , "Address, telephone data.".ikody                                                        as "Zip Code"
  , "Address, telephone data.".ipoczty                                                      as "Post"
  , "Address, telephone data.".imiejsco                                                     as "Town"
  , "Address, telephone data.".iwojewod                                                     as "Province"
  , "Address, telephone data.".ipowiaty                                                     as "District"
  , "Address, telephone data.".igminy                                                       as "Commune"
  , "Address, telephone data.".idgminy                                                      as "Previous id"
  , "Address, telephone data.".czy_adres                                                    as "Edit address"
  , "Address, telephone data.".adres                                                        as "Partner's data"
  , "Address, telephone data.".telefon                                                      as "Phone"
  , "Address, telephone data.".telefond                                                     as "Phone 2"
  , "Address, telephone data.".telefonk                                                     as "Phone 3"
  , "Address, telephone data.".telex                                                        as "Telex number"
  , "Address, telephone data.".telefax                                                      as "Fax number"
  , "Address, telephone data.".e_mail                                                       as "e-mail"
  , "Address, telephone data.".www                                                          as "Website"
  , "Address, telephone data.".czy_inna                                                     as "Edit name"
  , "Address, telephone data.".opis                                                         as "Address desc."
  , "Address, telephone data.".ikontrah1                                                    as "As a partner"
  , "Address, telephone data.".iln                                                          as "ILN"
  , "Address, telephone data.".islownik                                                     as "Index of description in a"
  , "Address, telephone data.".kto                                                          as "Who"
  , "Address, telephone data.".kiedy                                                        as "When"
  , "Address, telephone data.".minuta                                                       as "Time"
  , "Address, telephone data.".samofakt                                                     as "Self-invoicing?"
  , "Dictonary of partners".ukontrah                                                        as "Trading partner status"
  , "Dictonary of partners".ikontrah                                                        as "Partner ID"
  , "Dictonary of partners".izaklady                                                        as "Plant ID"
  , "Dictonary of partners".info                                                            as "Partner code"
  , "Dictonary of partners".iopis                                                           as "Unused field"
  , "Dictonary of partners".nip                                                             as "VAT ID"
  , "Dictonary of partners".nip10                                                           as "VAT ID (without breaks)"
  , "Dictonary of partners".platnikvat                                                      as "Sales document type"
  , "Dictonary of partners".uwagi                                                           as "Additional comments"
  , "Dictonary of partners".pocz_z                                                          as "Date of creating a partner"
  , "Dictonary of partners".opis                                                            as "Partner name"
  , "Dictonary of partners".opis2                                                           as "Additional description of Company"
  , "Dictonary of partners".fosoby                                                          as "Person"
  , "Dictonary of partners".flisty                                                          as "List of payments"
  , "Dictonary of partners".fplatnik                                                        as "Payer"
  , "Dictonary of partners".fzaloga                                                         as "Package users"
  , "Dictonary of partners".fplantat                                                        as "Planter"
  , "Dictonary of partners".finspekt                                                        as "Inspector"
  , "Dictonary of partners".wojskowy                                                        as "Covered by the obligation"
  , "Dictonary of partners".fprenum                                                         as "Subscriber"
  , "Dictonary of partners".fwysyl                                                          as "Shipping"
  , "Dictonary of partners".fzgoda                                                          as "Information about the sub"
  , "Dictonary of partners".bok                                                             as "Customer/Code 1"
  , "Dictonary of partners".bok2                                                            as "Customer/Code 2"
  , "Dictonary of partners".islownik                                                        as "Id of description in diff"
  , "Dictonary of partners".idowody_w                                                       as "Document archive id"
  , "Dictonary of partners".kto                                                             as "Who"
  , "Dictonary of partners".kiedy                                                           as "When"
  , "Dictonary of partners".minuta                                                          as "Time"
  , "Dictonary of partners"."timestamp"                                                     as "Time of last modification"
  , coalesce("Payment Terms".termin_def, '14')                                              as "Payment Terms Code"
  , coalesce("Payment Terms"."Payment Method", 'P')                                         as "Payment Method"
  , "Payment Terms".splatnex                                                                as "Payment Method2"
  , "Credit Limit".konc                                                                     as "Limit date"
  , "Credit Limit".kwota                                                                    as "Limit"
   , concat("Address, telephone data.".ikontrah,trim("Address, telephone data.".iadres)) as "CustID"
  , "KeyWords"."CustomerClassificationID"                                                   as "CustomerClassificationID"
  , "Segments"."SALESSEGMENTID"                                                             as "SALESSEGMENTID"
  , "Segments"."LINEOFBUSINESSID"                                                           as "LINEOFBUSINESSID"
  , "Segments"."COMPANYCHAIN"                                                               as "COMPANYCHAIN"
  , "Persons".isprzeda                                                                      as "Salespersone Code"
  , case
        when trim("Person Names".imie2) is null
            then concat(trim("Person Names".imie),' ', trim("Person Names".nazwisko))
            else concat(trim("Person Names".imie), ' ', trim("Person Names".imie2),' ', trim("Person Names".nazwisko))
    end                     as "Full Name"
  , "Person Names".nazwisko as "Last Name"
  , "Person Names".imie     as "First Name"
  , "Person Names".imie2    as "First Name2"
FROM
    fk.dfirmy as "Database of Partners"
    left join
        fk.dadres as "Address, telephone data."
        on
            "Address, telephone data.".ikontrah = "Database of Partners".ikontrah
    left join
        (
            select distinct
            on
                (
                    ikontrah
                )
                ikontrah
              , "Payment Conditions".termin_def
              , coalesce("Payment Conditions"."PM_1", 'P') as "Payment Method"
              , splatnex
            from
                fk.dplat_h
                left join
                    (
                        select
                            termin_def
                          , coalesce(iplatnex, 'P') as "PM_1"
                        from
                            fk.dplatnex
                    )
                    as "Payment Conditions"
                    on
                        "Payment Conditions"."PM_1" = fk.dplat_h.iplatnex
            where
                termin is not null
            order by
                ikontrah
              , Termin
              , konc desc
        )
        as "Payment Terms"
        on
            "Payment Terms".ikontrah = "Database of Partners".ikontrah
    right join
        (
            select distinct
                    concat(ikontrah, 'A') as "PrimaryKey_Invoice_B"
            from
                fk.dfaktury
            where
                ikontrah is not null
        )
        as "Sales Invoice_B"
        on
                concat("Address, telephone data.".ikontrah, iadres) = "Sales Invoice_B"."PrimaryKey_Invoice_B"
    left join
        fk.dkontrah as "Dictonary of partners"
        on
            "Dictonary of partners".ikontrah = "Database of Partners".ikontrah
    left join
        (
            select distinct
            on
                (
                    ikontrah
                )
                "CL".ikontrah
              , "CL".typumowy
              , "CL".konc
              , "CL".kwota
            FROM
                fk.dumowy as "CL"
            where
                typumowy    = 'K'
                or typumowy = 'k'
            order by
                ikontrah
              , konc desc
        )
        as "Credit Limit"
        on
            "Database of Partners".ikontrah = "Credit Limit".ikontrah
    left join
        (
            select
                    concat("KW".ikontrah, "KW".iadres) as "PrimaryKey"
              , 'Key Account'::text                    as "CustomerClassificationID"
            FROM
                fk.dhasla as "KW"
            where
                ihaslax     = 'H_key accounts'
                and ihaslas = '1'
                and konc   >= current_date
        )
        as "KeyWords"
        on
                concat("Address, telephone data.".ikontrah, iadres) = "KeyWords"."PrimaryKey"
    left join
        (
            select distinct
            on
                (
                    ikontrah
                  , iadres
                )
                    concat(ikontrah, trim(iadres)) as "PrimaryKey"
              , case
                    when ihaslax is not null
                        and ihaslax        = 'H_segmentacja'
                        and konc          >= current_date
                        then ihaslas
                end as "SALESSEGMENTID"
              , case
                    when ihaslax is not null
                        and ihaslax        = 'H_kategoria'
                        and konc          >= current_date
                        then ihaslas
                end as "LINEOFBUSINESSID"
              , case
                    when ihaslax is not null
                        and ihaslax        = 'O_kryt_2'
                        and konc          >= current_date
                        then ihaslas
                end as "COMPANYCHAIN"
            FROM
                fk.dhasla as "Seg"
            where
                ihaslax in ('H_segmentacja'
                          , 'H_kategoria'
                          , 'O_kryt_2')
        )
        as "Segments"
        on
                concat("Address, telephone data.".ikontrah, iadres) = "Segments"."PrimaryKey"
    left join
        fk.dadres_h as "Persons"
        on
                concat("Persons".ikontrah, trim("Persons".iadres)) = concat("Address, telephone data.".ikontrah,trim("Address, telephone data.".iadres))
    left join
        fk.dsprzeda as "Sales Persons"
        on
            "Sales Persons".isprzeda = "Persons".isprzeda
    left join
        fk.dosoby as "Person Names"
        on
            "Sales Persons".ikontrah = "Person Names".ikontrah
where
        concat("Address, telephone data.".ikontrah, "Address, telephone data.".iadres) <> ''
    and "Persons".konc                                                                 >= current_date
order by
    "PrimaryKey"
;